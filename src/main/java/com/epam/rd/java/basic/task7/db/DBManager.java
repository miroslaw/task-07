package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static final String CONNECTION_URL =
			"jdbc:mysql://localhost:3306/testdb"
					+ "?user=root&password=SOLne4naya70";
	private static final String SQL_FING_ALL_USERS =
			"SELECT * FROM users";
	private static final String SQL_INSERT_USER =
			"INSERT INTO users values (DEFAULT, ?)";
	private static final String SQL_INSERT_TEAM =
			"INSERT INTO teams values (DEFAULT, ?)";
	private static final String SQL_FING_ALL_TEAMS =
			"SELECT * FROM teams";
	private static final String SQL_SET_TEAMS_FOR_USER =
			"INSERT INTO users_teams values(? , ?)";
	private static final String SQL_GET_USER_TEAMS =
			"SELECT id, name FROM teams WHERE id IN " +
					"(SELECT team_id FROM users_teams WHERE user_id = ?)";
	private static final String SQL_DELETE_TEAM =
			"DELETE FROM teams WHERE id=?";
	private static final String SQL_FIND_TEAM_BY_ID =
			"SELECT * FROM teams WHERE id=?";
	private static final String SQL_GET_USER =
			"SELECT * FROM users WHERE login=?";
	private static final String SQL_GET_TEAM =
			"SELECT * FROM teams WHERE name=?";
	private static final String SQL_UPDATE_TEAM =
			"UPDATE teams SET name=? WHERE id=?";
	private static final String SQL_DELETE_USERS =
			"DELETE from users WHERE id=?";

	private static DBManager instance;

	static String getUrl(){

		try {
			return Files.readString(Path.of("app.properties")).substring(15);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static synchronized DBManager getInstance() {
		if (instance==null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {

		List<User> userList = new ArrayList<>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			con = getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FING_ALL_USERS);

			while(rs.next()) {
				userList.add(extractUser(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("DBerror", e);
		} finally {
			close(con);
		}


		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_INSERT_USER,
					Statement.RETURN_GENERATED_KEYS);

			int k=1;
			pstmt.setString(k++, user.getLogin());

			if(pstmt.executeUpdate() >0 ) {
				rs = pstmt.getGeneratedKeys();
				if(rs.next()) {
					user.setId(rs.getInt(1));
				}
			}


		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(con);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			pstmt = con.prepareStatement(SQL_DELETE_USERS);
			for(User user : users) {
				pstmt.setInt(1, user.getId());
				pstmt.executeUpdate();
			}

			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
			return false;
		}finally {
			close(con);
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		User user = null;
		try {
			con = getConnection();

			stmt = con.prepareStatement(SQL_GET_USER);
			stmt.setString(1, login);
			rs = stmt.executeQuery();

			while(rs.next()) {
				user = User.createUser(login);
				user.setId(rs.getInt("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Team team = null;
		try {
			con = getConnection();

			stmt = con.prepareStatement(SQL_GET_TEAM);
			stmt.setString(1, name);
			rs = stmt.executeQuery();

			while(rs.next()) {
				team = Team.createTeam(name);
				team.setId(rs.getInt("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {

		List<Team> teamList = new ArrayList<>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			con = getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FING_ALL_TEAMS);

			while(rs.next()) {
				teamList.add(extractTeam(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}


		return teamList;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_INSERT_TEAM,
					Statement.RETURN_GENERATED_KEYS);

			int k=1;
			pstmt.setString(k++, team.getName());

			if(pstmt.executeUpdate() >0 ) {
				rs = pstmt.getGeneratedKeys();
				if(rs.next()) {
					team.setId(rs.getInt(1));
				}
			}


		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(con);
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			pstmt = con.prepareStatement(SQL_SET_TEAMS_FOR_USER);
			for(Team team : teams) {
				pstmt.setInt(1, user.getId());
				pstmt.setInt(2, team.getId());
				System.out.println(user.getId()+" : " +team.getId());
				pstmt.executeUpdate();
			}

			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
			throw new DBException("DBerror", e);
		}finally {
			close(con);
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> listTeam = new ArrayList<>();
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			con = getConnection();

			stmt = con.prepareStatement(SQL_GET_USER_TEAMS);
			stmt.setInt(1, user.getId());
			rs = stmt.executeQuery();

			while(rs.next()) {
				listTeam.add(extractTeam(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("DBerror", e);
		} finally {
			close(con);
		}

		return listTeam;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_TEAM);
			pstmt.setInt(1, team.getId());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(con);
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_TEAM);

			pstmt.setString(1, team.getName());
			pstmt.setInt(2, team.getId());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(con);
		}
		return true;
	}

	private User extractUser(ResultSet rs) throws SQLException {
		User user = User.createUser(rs.getString("login"));
		user.setId(rs.getInt("id"));
		return user;
	}

	private Team extractTeam(ResultSet rs) throws SQLException {
		Team team = Team.createTeam(rs.getString("name"));
		team.setId(rs.getInt("id"));
		return team;
	}

	public Team getTeamById(ResultSet rs) throws SQLException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet res = null;
		Team team = null;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_FIND_TEAM_BY_ID);
			pstmt.setInt(1, rs.getInt("team_id"));
			res = pstmt.executeQuery();

			while(res.next()) {
				team = Team.createTeam(res.getString("name"));
				team.setId(res.getInt("team_id"));
				System.out.println(team.getId()+" :: "+team.getName());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}

		return team;
	}



	public static Connection getConnection() throws SQLException {
		Connection con = DriverManager.getConnection(getUrl());
		//con.setAutoCommit(false);
		return con;
	}

	public static void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void rollback(Connection con) {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}